package com.example.network.data

import com.example.network.model.Post
import com.example.network.network.RetrofitClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PostRepository {
    private val postApi = RetrofitClient.postApi

    fun getPosts(): Flow<List<Post>> {
        return flow {
            val posts = postApi.getPosts()
            emit(posts)
        }
    }

    fun updatePost(post: Post): Flow<Post> {
        return flow {
            val updatedPost = postApi.updatePost(post.id, post)
            emit(updatedPost)
        }
    }
}