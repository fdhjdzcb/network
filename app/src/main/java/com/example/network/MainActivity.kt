package com.example.network

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.network.data.PostRepository
import com.example.network.model.Post
import com.example.network.ui.theme.NetworkTheme
import com.example.network.viewmodel.PostViewModel
import com.example.network.viewmodel.PostViewModelFactory


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NetworkTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    val postViewModel: PostViewModel = viewModel(
                        factory = PostViewModelFactory(PostRepository())
                    )
                    PostsContent(postViewModel)
                }
            }
        }
    }
}

@Composable
fun PostsContent(postViewModel: PostViewModel) {
    val posts by postViewModel.posts.collectAsState()
    val selectedPost by postViewModel.selectedPost.collectAsState()
    val isEditing by postViewModel.isEditing.collectAsState()

    PostsScreen(
        posts = posts,
        selectedPost = selectedPost,
        isEditing = isEditing,
        onPostClick = { postViewModel.selectPost(it) },
        onEditClick = { postViewModel.toggleEditing() },
        onTitleChange = { postViewModel.updateTitle(it) },
        onBodyChange = { postViewModel.updateBody(it) },
        onSaveClick = { postViewModel.savePost() }
    )
}


@Composable
fun PostsScreen(
    posts: List<Post>,
    selectedPost: Post?,
    isEditing: Boolean,
    onPostClick: (Post) -> Unit,
    onEditClick: () -> Unit,
    onTitleChange: (String) -> Unit,
    onBodyChange: (String) -> Unit,
    onSaveClick: () -> Unit
) {
    Column(Modifier.fillMaxSize()) {
        PostDetails(
            post = selectedPost,
            isEditing = isEditing,
            onEditClick = onEditClick,
            onTitleChange = onTitleChange,
            onBodyChange = onBodyChange,
            onSaveClick = onSaveClick
        )
        PostsList(
            posts = posts,
            selectedPost = selectedPost,
            onPostClick = onPostClick
        )
    }
}

@Composable
fun PostsList(
    posts: List<Post>,
    selectedPost: Post?,
    onPostClick: (Post) -> Unit
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        item {
            Text(
                text = stringResource(R.string.posts),
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(bottom = 8.dp)
            )
        }
        items(posts) { post ->
            PostItem(
                post = post,
                isSelected = post == selectedPost,
                onClick = { onPostClick(post) }
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}

@Composable
fun PostItem(
    post: Post,
    isSelected: Boolean,
    onClick: () -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(10.dp),
        colors = if (isSelected) CardDefaults.cardColors(Color(0xFF6FFD2D)) else CardDefaults.cardColors(Color(0xFFBCE9A8))
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            Text(
                text = "Пост №${post.id}",
                style = if (isSelected) {
                    TextStyle(
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp
                    )
                } else {
                    TextStyle.Default
                },
                maxLines = 1
            )
            Text(
                text = post.title,
                style = if (isSelected) {
                    TextStyle(
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp
                    )
                } else {
                    TextStyle.Default
                },
                maxLines = 1
            )
        }
    }
}

@Composable
fun PostDetails(
    post: Post?,
    isEditing: Boolean,
    onEditClick: () -> Unit,
    onTitleChange: (String) -> Unit,
    onBodyChange: (String) -> Unit,
    onSaveClick: () -> Unit
) {
    Column(
        modifier = Modifier.padding(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.post_details),
            fontWeight = FontWeight.Bold
        )

        post?.let {
            if (isEditing) {
                EditablePostDetails(post = post, onTitleChange = onTitleChange, onBodyChange = onBodyChange, onSaveClick = onSaveClick)
            } else {
                ViewPostDetails(post = post, onEditClick = onEditClick)
            }
        } ?: run {
            Text(text = stringResource(R.string.none_selected))
        }
    }
}

@Composable
fun EditablePostDetails(post: Post, onTitleChange: (String) -> Unit, onBodyChange: (String) -> Unit, onSaveClick: () -> Unit) {
    Column(modifier = Modifier.fillMaxWidth()) {
        TextField(
            value = post.title,
            onValueChange = onTitleChange,
            label = { Text("Title") },
            modifier = Modifier
                .fillMaxWidth()
                .background(Color(0xFF3C642A))
        )
        TextField(
            value = post.body,
            onValueChange = onBodyChange,
            label = { Text("Body") },
            modifier = Modifier
                .fillMaxWidth()
        )
        Button(
            onClick = onSaveClick,
            colors = ButtonDefaults.buttonColors(Color(0xFF3C642A)),
            modifier = Modifier.align(Alignment.CenterHorizontally)

        ) {
            Text(stringResource(R.string.save))
        }
    }
}

@Composable
fun ViewPostDetails(post: Post, onEditClick: () -> Unit) {
    Column(modifier = Modifier.fillMaxWidth()) {
        Text(text = stringResource(R.string.title), fontWeight = FontWeight.Bold)
        Text(text = post.title)

        Text(text = stringResource(R.string.body), fontWeight = FontWeight.Bold)
        Text(text = post.body)

        Spacer(modifier = Modifier.padding(vertical = 10.dp))

        Button(
            onClick = onEditClick,
            colors = ButtonDefaults.buttonColors(Color(0xFF3C642A)),
            modifier = Modifier.align(Alignment.CenterHorizontally)
                .clip(RoundedCornerShape(4.dp))
        ) {
            Text(stringResource(R.string.edit))
        }
    }
}